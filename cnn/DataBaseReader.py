#-*- coding: utf-8 -*-

import pandas as pd
import numpy as np


class DataBaseReader:
	def __init__(self, dataBaseFile):
		nms = ['Time','RelativeTimeMilliseconds','Clock','HR','ST-II','Pulse','SpO2','Perf','etCO2','imCO2','awRR','NBP (Sys)','NBP (Dia)','NBP (Mean)','NBP (Pulse)','NBP (Time Remaining)','ART (Sys)','ART (Dia)','ART (Mean)','etDES','inDES','etISO','inISO','etSEV','inSEV','etN2O','inN2O','MAC','etO2','inO2','Temp','BIS','SQI','EMG','Tidal Volume','Minute Volume','RR','Set Tidal Volume','Set RR','Set I:E Ratio','Set PEEP','Set PAWmax','Set PAWmin','Set Mechanical Ventilation','Tidal Volume Exp (Spiro)','Tidal Volume In (Spiro)','Minute Volume Exp (Spiro)','Minute Volume In (Spiro)','Lung Compliance (Spiro)','Airway Resistance (Spiro)','Max Inspiratory Pressure (Spiro)','ECG','Pleth','CO2','ART','EEG','AWP','AWF','AWV','AWP-Spiro','AWF-Spiro','AWV-Spiro','Num Patient Alarms','Num Technical Alarms','Alarms...']#...,'Alarms...2','Alarms...3','Alarms...4','Alarms...5','Alarms...6']
		self.db = pd.read_csv(dataBaseFile, sep=',',names=nms,  low_memory=False)
		
	def getPPG(self):
		val = self.db['Pleth'].values			
		val = np.delete(val, 0)
		val = val.astype(np.float)
		print val
		return val			
	def getPd(self):
		
		val =  self.db['NBP (Dia)'].values
                val = np.delete(val, 0)
		val = val.astype(np.float)
		print val
		return val
	def getPs(self):
		val = self.db['NBP (Sys)'].values
		val = np.delete(val, 0)
		val = val.astype(np.float)
		print val
		return val
