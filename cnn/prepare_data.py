#-*- coding: utf-8 -*-
import DataBaseReader
import numpy as np
import os
import math

outFileName = 'prepareDataTest_16.csv'
path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test16/"
files = os.listdir(path)
features = []#np.array([])
isFirst = True
for f in files:
	
	dataBaseReader = DataBaseReader.DataBaseReader(path + f)
	print f
	PPG = dataBaseReader.getPPG()
	Pd = dataBaseReader.getPd()
	Ps = dataBaseReader.getPs()
	
	for i in range(len(PPG)):
		feature = []
		if (not math.isnan(Pd[i]) and not math.isnan(Ps[i]) and not math.isnan(PPG[i])):
			feature.append(Pd[i])
			feature.append(Ps[i])
			feature.append(PPG[i])
			features.append(feature)


	
	
	
#print features
np.savetxt(outFileName, features, delimiter=',',newline='\n')
