import numpy as np
import os
import sys
import argparse
import glob
import time
import csv
import caffe
import matplotlib.pyplot as plt

# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 900

# Find the mean for each dimension of the samples
# Returns a vector of means with dim same as the data
def find_mean(sample_list):
    if not sample_list:
        return []

    mean_list = [0] * len(sample_list[0])
    for sample in sample_list:
        # Element-wise sum
        mean_list = map(sum, zip(mean_list, sample))
    
    return (np.array(mean_list) / len(sample_list)).tolist()



def process_data_theirs(data_path):

    current_sample_x = []



    with open(data_path, 'rt') as f:
        reader = csv.reader(f)
        prev_label1 = " "
        prev_label2 = " "
        for row in reader:
            if not row:
                continue
            label1 = int(float(row[0]))
            label2 = int(float(row[1]))
            x = float(row[2])

            # Reset when new label is found
            if (prev_label1 != label1 or prev_label2 != label2):
                prev_label1 = label1
                prev_label2 = label2
                current_sample_x = []

            current_sample_x.append(x)

            # Store when we have the length
            if (len(current_sample_x) >= width):
                
                print label1
                print label2
                ax = plt.gca()
                ax.set_ylim([-1, 1])
                plt.plot(current_sample_x)
		print current_sample_x
                
                plt.show()
                # sample_list.append(current_sample)
                # label_list.append(label)
                # Shrink current sample based on the overlap ratio
                current_sample_x = current_sample_x[int(len(current_sample_x) * (1.0-percent_overlap)):]


def main():
    data_path = 'prepareDataTest_all0.csv'

    process_data_theirs(data_path)

if __name__ == '__main__':
    main()