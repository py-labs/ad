# Converts dataset into HDF5 format to be fed into caffe
import os
import numpy as np
import h5py
import csv
import sys
import random
import caffe

SEED = 163


channels = ['x']
data_path = "prepareDataTest_all0.csv"
dataset_prefix = 'pressureEstimator_'
to_shuffle = True
zero_mean = False
merge_datasets = False

# Percent overlap between subsequent samples
percent_overlap = 0.50

file_dir = os.path.dirname(os.path.abspath(__file__))

# Caffe blobs have the dimensions (n_samples, n_channels, height, width)
num_channels = 1
height = 1
width = 900

# Trim the front of our dataset a few seconds because it time to take phone in / out of pocket
num_seconds_skipped = 3

# Evenly slice a list into equal proportions
# Source: http://stackoverflow.com/questions/4119070/how-to-divide-a-list-into-n-equal-parts-python
def slice_list(input, size):
    input_size = len(input)
    slice_size = input_size / size
    remain = input_size % size
    result = []
    iterator = iter(input)
    for i in range(size):
        result.append([])
        for j in range(slice_size):
            result[i].append(iterator.next())
        if remain:
            result[i].append(iterator.next())
            remain -= 1
    return result


def process_data_theirs(data_path):
    sample_list_x = [] # List of lists

    current_sample_x = []

    label_list1  = []
    label_list2  = []

    with open(data_path, 'rt') as f:
        reader = csv.reader(f)
        prev_label1 = " "
        prev_label2 = " "
        for row in reader:
            if not row:
                continue
            label1 = int(float(row[0]))
            label2 = int(float(row[1]))
            x = float(row[2])

            # Reset when new label is found
            if (prev_label1 != label1 or prev_label2 != label2):
                prev_label1 = label1
                prev_label2 = label2
                current_sample_x = []


            current_sample_x.append(x)

            # Store when we have the length
            if (len(current_sample_x) == width):
                sample_list_x.append(current_sample_x)
                label_list1.append(label1)
                label_list2.append(label2)

                # Retain only certain overlap percentages of the sample
                current_sample_x = current_sample_x[int(len(current_sample_x) * (1.0-percent_overlap)):]

    return sample_list_x, label_list1, label_list2

# Find the mean for each dimension of the samples
# Returns a vector of means with dim same as the data
def find_mean(sample_list):
    if not sample_list:
        return []

    mean_list = [0] * len(sample_list[0])
    for sample in sample_list:
        # Element-wise sum
        mean_list = map(sum, zip(mean_list, sample))

    return (np.array(mean_list) / len(sample_list)).tolist()


def main():
    # Load our data stuff
    project_root = "E:/CARS/PRESSURE/ad/cnn/"
    hdf5_path = project_root + 'hdf5/'

    # Load data
    sample_list_x, label_list1, label_list2 = process_data_theirs(data_path)

    num_samples = len(label_list1)

    # print(len(sample_list_x))
    # print(len(sample_list_y))
    # print(len(sample_list_z))
    # print(len(label_list))

    # Calculate HDF5 shape
    total_size = num_samples * num_channels * height * width
    print("Total size: {} \t HDF5 shape: ({}, {}, {}, {})".format(total_size, num_samples, num_channels, height, width))
    print sample_list_x
    # Zero mean
    if zero_mean:
        mean_x = find_mean(sample_list_x)
        print("Subtracting mean...")
        for i in range(len(sample_list_x)):
            sample_list_x[i][:] = np.subtract(sample_list_x[i], mean_x).tolist()
    print sample_list_x
    # Shuffle!
    random.seed(SEED)
    shuffled_index = range(num_samples)
    if to_shuffle:
        print("Shuffling...")
        random.shuffle(shuffled_index)

    sample_list_shuffled_x = []
    label_list1_shuffled  = []
    label_list2_shuffled = []
    for i in shuffled_index:
        sample_list_shuffled_x.append(sample_list_x[i])
        label_list1_shuffled.append(label_list1[i])
        label_list2_shuffled.append(label_list2[i])


    print("Shuffled size: {} \t Shape: ({}, {}, {}, {})".format(total_size, num_samples, num_channels, height, width))

    # Do 5-fold divide
    sample_list_folds_x = slice_list(sample_list_shuffled_x, 10)
    label_list1_folds  = slice_list(label_list1_shuffled, 10)
    label_list2_folds  = slice_list(label_list2_shuffled, 10)


    # Save data in HDFS format
    print("{} fold cross validation setup".format(len(label_list1_folds)))
    print("{} fold cross validation setup".format(len(label_list2_folds)))
    for i in range(len(label_list1_folds)):
        print("Fold {}".format(i))
        train_fold_index_list1 = range(len(label_list1_folds))
        train_fold_index_list1.pop(i)
        
        train_fold_index_list2 = range(len(label_list2_folds))
        train_fold_index_list2.pop(i)

        # Merging all training sets together
        train_x = []
        train_label1 = []
        train_label2 = []
        for j in train_fold_index_list1:
            train_x.extend(sample_list_folds_x[j])
            train_label1.extend(label_list1_folds[j])
            train_label2.extend(label_list2_folds[j])

        test_x = []
        test_label1 = []
        test_label2 = []

        test_x.extend(sample_list_folds_x[i])
        test_label1.extend(label_list1_folds[i])
        test_label2.extend(label_list2_folds[i])

        print("Train size: {}\t Test size: {}".format(len(train_label1), len(test_label1)))

        channel_index = 0


        # Formatting to HDF5
        # Train
        # (num_samples, num_channels, height, width)
        data1 = np.arange(len(train_label1) * num_channels * height * width)
        data1 = data1.reshape(len(train_label1), num_channels, height, width)
        data1 = data1.astype('float32')

        data_label1 = np.arange(len(train_label1))[:, np.newaxis]
        data_label1 = data_label1.astype('int32')
        
        data2 = np.arange(len(train_label2) * num_channels * height * width)
        data2 = data2.reshape(len(train_label2), num_channels, height, width)
        data2 = data2.astype('float32')

        data_label2 = np.arange(len(train_label2))[:, np.newaxis]
        data_label2 = data_label2.astype('int32')

        channel_dict = {
            'x':train_x,
        }
        for channel, channel_list in channel_dict.iteritems():
            output_path = str(i) + '/' + dataset_prefix + channel
            for j in range(len(train_label1)):
                data1[j][channel_index][0] = np.array(channel_list[j])
                data_label1[j] = np.array([train_label1[j]])

            with h5py.File(hdf5_path + output_path + '_1_train_data.h5', 'w') as f:
                f['data_' + channel] = data1
                f['label_' + channel] = data_label1
            with open(hdf5_path + output_path + '_1_train_data_list.txt', 'w') as f:
                f.write(hdf5_path + output_path + '_1_train_data.h5\n')
            print(data1.shape)
            print(data_label1.shape)
            
        for channel, channel_list in channel_dict.iteritems():
            output_path = str(i) + '/' + dataset_prefix + channel
            for j in range(len(train_label2)):
                data2[j][channel_index][0] = np.array(channel_list[j])
                data_label2[j] = np.array([train_label2[j]])

            with h5py.File(hdf5_path + output_path + '_2_train_data.h5', 'w') as f:
                f['data_' + channel] = data2
                f['label_' + channel] = data_label2
            with open(hdf5_path + output_path + '_2_train_data_list.txt', 'w') as f:
                f.write(hdf5_path + output_path + '_2_train_data.h5\n')
            print(data2.shape)
            print(data_label2.shape)


        # Test
        # (num_samples, num_channels, height, width)
        print(len(test_label1))
        test2 = np.arange(len(test_label2) * num_channels * height * width)
        test2 = test2.reshape(len(test_label2), num_channels, height, width)
        test2 = test2.astype('float32')

        test_label2 = np.arange(len(test_label2))[:, np.newaxis]
        test_label2 = test_label2.astype('int32')
        
        test1 = np.arange(len(test_label1) * num_channels * height * width)
        test1 = test1.reshape(len(test_label1), num_channels, height, width)
        test1 = test1.astype('float32')

        test_label1 = np.arange(len(test_label1))[:, np.newaxis]
        test_label1 = test_label1.astype('int32')

        channel_dict = {
            'x':test_x,
        }
        for channel, channel_list in channel_dict.iteritems():
            output_path = str(i) + '/' + dataset_prefix + channel
            for j in range(len(test_label2)):
                test2[j][channel_index][0] = np.array(channel_list[j])
                test_label2[j] = np.array([test_label2[j]])

            with h5py.File(hdf5_path + output_path + '_2_test_data.h5', 'w') as f:
                f['data_' + channel] = test2
                f['label_' + channel] = test_label2
            with open(hdf5_path + output_path + '_2_test_data_list.txt', 'w') as f:
                f.write(hdf5_path + output_path + '_2_test_data.h5\n')
            print(test2.shape)
            print(test_label2.shape)
            
            for j in range(len(test_label1)):
                test1[j][channel_index][0] = np.array(channel_list[j])
                test_label1[j] = np.array([test_label1[j]])

            with h5py.File(hdf5_path + output_path + '_1_test_data.h5', 'w') as f:
                f['data_' + channel] = test1
                f['label_' + channel] = test_label1
            with open(hdf5_path + output_path + '_1_test_data_list.txt', 'w') as f:
                f.write(hdf5_path + output_path + '_1_test_data.h5\n')
            print(test1.shape)
            print(test_label1.shape)



    print("Done.")

if __name__ == "__main__":
    main()