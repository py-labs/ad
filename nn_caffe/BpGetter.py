#-*- coding: utf-8 -*-
import pickle
import thinkdsp
import numpy as np
from numpy import NaN, Inf, arange, isscalar, asarray, array
from matplotlib.pyplot import plot, scatter, show
import math
import caffe
from glob import glob
import cv2
from sklearn.ensemble import RandomForestRegressor
from functions import *
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib as mpl


class BpGetter:
	def __init__(self, modelFile = 'trained_net_01_900_0001_5000', Fs = 100):
		
		#self.modelFile = modelFile
		self.Fs = Fs
		## getting neural trained data
		#fileObject = open(self.modelFile,'r')
		#fileObject = open('trained_net_0001','r')
		#self.net = pickle.load(fileObject)		
		self.net_pretrained = './models/spec_regression_VGG_small/spec_regression_VGG_small_iter_212352.caffemodel'
		self.net_model_file = './models/spec_regression_VGG_small/spec_regression_VGG_small_deploy_fc6.prototxt'
		self.net = caffe.Classifier(self.net_model_file, self.net_pretrained,
                               mean=None,
                               channel_swap=(2,1,0),
                               raw_scale=255,
                               image_dims=(224, 224))
		caffe.set_mode_gpu()
		filename0 = 'finalized_model_all_rbf_randomforest_every_fc6_separately0.sav'
		filename1 = 'finalized_model_all_rbf_randomforest_every_fc6_separately1.sav'
		self.model0 = pickle.load(open(filename0, 'rb'))
		self.model1 = pickle.load(open(filename1, 'rb'))




	def save_tmp_picture(self, signal, frame_rate):
	
	
		size = [224]  # PAA size
		quantile = [16] # Quantile size
		reduction_type = 'patch' # Reduce the image size using: full, patch, paa



	
	
		for s in size:  
        		for Q in quantile:
				#print 'read file', datafile, 'size',s
				raw = [signal]
				#raw = [map(float, each.strip().split()) for each in raw]
				length = len(raw[0])-1
            
				#print 'format data'
				label = []
				paaimage = []
				paamatrix = []
				patchimage = []
				patchmatrix = []
				fullimage = []
				fullmatrix = []
				each = signal
				if True:
				#for each in raw:
					#label.append(each[0])
					#std_data = rescaleminus(each[1:])
					#std_data = rescale(each[1:])
					std_data = each[1:]
					#std_data = standardize(each[1:])
					#std_data = rescaleminus(std_data)
                
					paalist = paa(std_data,s,None) 
                
					############### Markov Matrix #######################
					mat, matindex, level = QMeq(std_data, Q)
					##paamat,paamatindex = QMeq(paalist,Q)
					paamatindex = paaMarkovMatrix(paalist, level)
					column = []
					paacolumn = []
					for p in range(len(std_data)):
						for q in range(len(std_data)):
							column.append(mat[matindex[p]][matindex[(q)]])
                        
					for p in range(s):
						for q in range(s):
							paacolumn.append(mat[paamatindex[p]][paamatindex[(q)]])
                        
					column = np.array(column)
					columnmatrix = column.reshape(len(std_data),len(std_data))
					fullmatrix.append(column)
					paacolumn = np.array(paacolumn)
					paamatrix.append(paacolumn)
                
					fullimage.append(column.reshape(len(std_data),len(std_data)))
					paaimage.append(paacolumn.reshape(s,s))
                
					batch = len(std_data)/s
					patch = []
					for p in range(s):
						for q in range(s):
							patch.append(np.mean(columnmatrix[p*batch:(p+1)*batch,q*batch:(q+1)*batch]))
					patchimage.append(np.array(patch).reshape(s,s))
					patchmatrix.append(np.array(patch))
 
				paaimage = np.asarray(paaimage)
				paamatrix = np.asarray(paamatrix)
				patchimage = np.asarray(patchimage)
				patchmatrix = np.asarray(patchmatrix)
	            		fullimage = np.asarray(fullimage)
        	    		fullmatrix = np.asarray(fullmatrix)
            			label = np.array(label)
	                        #print fullimage[0]
				#print len(fullimage[0][0])
				if reduction_type == 'patch':
					savematrix = patchmatrix
				elif reduction_type == 'paa':
					savematrix = paamatrix
				else:
					savematrix = fullmatrix
                	
				#datafilename = datafile +'_'+reduction_type+'_PAA_'+str(s)+'_Q_'+str(Q)+'_MTF'
				#pickledata(savematrix, label, train, datafilename)
				
	                        #sp.misc.imsave('outfile_patch.png',patchmatrix )
				#sp.misc.imsave('outfile_paa.png', paamatrix)
				fff = sp.misc.toimage(fullimage[0])
				sp.misc.imsave('outfile_full.png', fff )
				#plt.imshow(fullimage[0])
				#plt.show()
	                        #filename = path_end0 + "/full/" + path_end1
				#mpl.pyplot.imsave(fname=filename,
                	        #  arr=fullimage[0],
	                        #  cmap='jet')
				#print "333"
				filename = "tmp.jpg"
				mpl.pyplot.imsave(fname=filename,
                        	  arr=patchimage[0],
	                          cmap='jet')
				#filename = path_end0 + "/paa/" + path_end1
				#mpl.pyplot.imsave(fname=filename,
                        	#  arr=paaimage[0],
	                        #  cmap='jet')


	def getBP(self, ppg, scaleFactor = 1/1000, peakMaximum = 0.1): #1/4500
		y = []
		for p in ppg:
			y.append(p*scaleFactor)
		
		maxtab, mintab = self.peakdet(y, peakMaximum)
		plot(y)
		#print maxtab
		difference = 0
		if (len(maxtab > 0)):
		    	#scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
		    	#scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
    			#show()
	    		difference = self.getDiffenence(mintab, maxtab)
			print "difference: " + str(difference)
		bp1_dia = []
		bp1_sys = []
		#if (pout[0] >= 0.01 and pout[0] <=1 and difference >= 0.1 and difference <= 0.6):
		if (difference >= 0.1 and difference <= 0.6):
			#start BP
			peakid = []
			troughid = []
			len1 = min(len(maxtab), len(mintab))
			for i in range(len1 - 1):
				peakid.append(int(maxtab[i+1][0]))
				troughid.append(int(mintab[i][0]))
			kkk = 0

			for j in range(len(peakid)):
				if (j%2 == 0 and j+30 < len(peakid)):
					#print peakid[j+30]
					#print peakid[j]
					#print len(y)	
					sample_x = y[peakid[j]:peakid[j+30]]
							
				
					#try:
					if (kkk < 35):
						#print "1"
						#print len(sample_x)
						self.save_tmp_picture(sample_x, self.Fs)
						#print '2'
						img1 = cv2.imread("tmp.jpg")    
						#print '3'
						img = cv2.resize(img1, (224, 224))    
						input_image = img / 255.0
					        input_image = input_image[:,:,(2,1,0)]
					 	prediction = self.net.predict([input_image])[0].reshape(1,-1)
 						pred0 = self.model0.predict(prediction)
						pred1 = self.model1.predict(prediction)
						print "predict: ", pred0, pred1			
						bp1_sys.append(pred0[0])
						bp1_dia.append(pred1[0])
					#except:
					#	print "Exception"
					kkk+=1
					
				

		#else:
		#	bp1_dia = 0
		#	bp1_sys = 0
		

		return bp1_sys, bp1_dia

		
	
	def peakdet(self, v, delta, x = None):
		maxtab = []
		mintab = []
		   
		if x is None:
			x = arange(len(v))
		
		v = asarray(v)
		
		if len(v) != len(x):
			sys.exit('Input vectors v and x must have same length')
		
		if not isscalar(delta):
			sys.exit('Input argument delta must be a scalar')
		
		if delta <= 0:
			sys.exit('Input argument delta must be positive')
		
		mn, mx = Inf, -Inf
		mnpos, mxpos = NaN, NaN
		
		lookformax = True
		
		for i in arange(len(v)):
			this = v[i]
			if this > mx:
				mx = this
				mxpos = x[i]
			if this < mn:
				mn = this
				mnpos = x[i]
		    	
			if lookformax:
				if this < mx-delta:
					maxtab.append((mxpos, mx))
					mn = this
					mnpos = x[i]
					lookformax = False
			else:
				if this > mn+delta:
					mintab.append((mnpos, mn))
					mx = this
					mxpos = x[i]
					lookformax = True

		return array(maxtab), array(mintab)

	
	
    	def getDiffenence(self,mintab, maxtab):
		countMax = len(maxtab)
		countMin = len(mintab)
		
		
		countDiff = 0
		sumDiff = 0
		for i in range(min(countMax, countMin)):
			countDiff += 1
			sumDiff	+= abs(maxtab[i][1] - mintab[i][1])
		
		if (countDiff != 0 and min(countMax, countMin) > 5):
			
			return (float)(sumDiff)/countDiff
		#print countMax
		#print countMin
		#arr = []
		#for i in range(min(countMax, countMin)):
		#	arr.append(abs(maxtab[i][1] - mintab[i][1]))
		#print arr			

		return 0

