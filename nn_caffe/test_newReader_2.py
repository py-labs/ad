import TextBaseReader
import BpGetter
import numpy as np
import thinkdsp
import math
import cmath
import pickle



# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 1500



path = "E:/CARS/PRESSURE/test_data/2017-01-11-14-00-53.csv"
path = "E:/CARS/PRESSURE/test_data/2017-01-11-12-37-30.csv"

#path = "E:/CARS/PRESSURE/Pulse-out/2017-03-17-11-04-05.csv"
#path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test15/uq_vsd_case31_fulldata_11.csv"
textBaseReader = TextBaseReader.TextBaseReader(path)
PPG = textBaseReader.getPPG()
#bpGetter = BpGetter.BpGetter('trained_net_01_900_0001_5000', 100)
bpGetter = BpGetter.BpGetter('trained_net_01_900_0001_5000_UCI', 100)
bp1_sys,bp1_dia = bpGetter.getBP(PPG, 1/4500.0, 0.1)
print bp1_sys
print bp1_dia
#process_data_theirs(PPG, width, bpGetter)