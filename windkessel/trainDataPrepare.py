#-*- coding: utf-8 -*-
import DataBaseReader
import FeatureGetter
import numpy as np
import os

outFileName = 'prepareDataTest_all.out'
path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test/"
files = os.listdir(path)
features = np.array([])
isFirst = True
for f in files:
	
	dataBaseReader = DataBaseReader.DataBaseReader(path + f)
	print f
	PPG = dataBaseReader.getPPG()
	Pd = dataBaseReader.getPd()
	Ps = dataBaseReader.getPs()
	#print PPG
	#print Pd
	#print Ps

	featureGetter = FeatureGetter.FeatureGetter(PPG, Pd, Ps, 10, 0.15, False)
	features_i = featureGetter.getFeatures()

	if (isFirst and len(features_i) != 0):
		features = features_i
		isFirst = False
	else:
		if (len(features_i) != 0):
			features = np.concatenate((features, features_i))
	
	
	
print features
np.savetxt(outFileName, features, delimiter=',',newline='\n', header="Ts,Td,B25,B50,B75,c6,c5,c4,c3,c2,c1,c0,Pd,Ps,L3,L4")
