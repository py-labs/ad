#-*- coding: utf-8 -*-
import DataBaseReader
import FeatureGetter
import numpy as np
import os
import pickle
import pandas as pd
from pandas import read_csv, DataFrame
from sklearn.cross_validation import train_test_split

outFileName = 'predictedDataTestxgb10_800_006_202_PD.out'
path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test/"
files = os.listdir(path)
features = np.array([])
isFirst = True

filenamePd = 'finalized_modelPs_xgb10_800_006_202.sav'
modelPd = pickle.load(open(filenamePd, 'rb'))

filenamePs = 'finalized_modelPs_xgb10_800_006_202.sav'
modelPs = pickle.load(open(filenamePs, 'rb'))

dataset = read_csv('prepareDataTrain.csv',',')
dataset = dataset.drop(['L3','L4'], axis=1)
trgPd = dataset['Pd']
trgPs = dataset['Ps']
trn = dataset.drop(['Pd','Ps'], axis=1)


Xtrn, Xtest, Ytrn, Ytest = train_test_split(trn, trgPs, test_size=0.00002)
print Xtest
modelPs.predict(Xtest)

for f in files:
	
	dataBaseReader = DataBaseReader.DataBaseReader(path + f)
	print f
	PPG = dataBaseReader.getPPG()
	Pd = dataBaseReader.getPd()
	Ps = dataBaseReader.getPs()
	#print PPG
	#print Pd
	#print Ps

	featureGetter = FeatureGetter.FeatureGetter(PPG, Pd, Ps, 10, 0.15, False)
	features_i = featureGetter.getFeatures()
	features_i_new = np.array([])
	if (isFirst):
		Pd_new = 80
		#isFirst = False
	for feature in features_i:
		f_part = feature
		
		f_part = np.delete(f_part, len(f_part)-1, 0)
		f_part = np.delete(f_part, len(f_part)-1, 0)
		f_part = np.delete(f_part, len(f_part)-1, 0)
		f_part = np.delete(f_part, len(f_part)-1, 0)


		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)
		#f_part = np.delete(f_part, len(f_part)-1, 0)


		f_part0 = f_part.reshape(1,-1)
		frame_part = pd.DataFrame(f_part0, columns=['# Ts','Td','B25','B50','B75','c6','c5','c4','c3','c2','c1','c0'])
		#print frame_part
		#print "trtr"
		#print frame_part.name
                #print type(dataset)
		#print type(frame_part)

		#L3 = modelPd.predict(f_part0)
		#L4 = modelPs.predict(f_part0)
		#print L3
		#print L4
		
		
		#Ts = f_part[0]
		#Td = f_part[1]
		
		
		#Ps_new = Pd_new*np.e**(-Ts/L4) + L3*Ts*L4*(1+np.e**(-Ts/L4))/(Ts**2 + L4**2*np.pi**2)
		#Pd_new = Ps_new*np.e**(-Td/L4)
		#print str(Pd_new) + ' ' + str(Ps_new) + ' ' + str(L3) + ' ' + str(L4)
		
		L3 = [-1]
		L4 = [-1]
		#Pd_new = modelPd.predict(f_part0)
		#Ps_new = modelPs.predict(f_part0)	
		Pd_new = modelPd.predict(frame_part)
		Ps_new = modelPs.predict(frame_part)	
		

		f_part = np.concatenate((f_part, Pd_new))
		f_part = np.concatenate((f_part, Ps_new))
		
		f_part = np.concatenate((f_part, L3))
		f_part = np.concatenate((f_part, L4))
		feature = f_part
		#print feature
		
		features_i_new = np.vstack([features_i_new, feature]) if features_i_new.size else feature
		#features_i_new.append(feature)
		#print features_i_new
		#print features_i
	
	#features = np.vstack([features, features_i_new]) if features.size else features_i_new
	#print features
	if (isFirst and len(features_i_new) != 0):
		features = features_i_new
		isFirst = False
	else:
		if (len(features_i_new) != 0):
			features = np.concatenate((features, features_i_new))
np.savetxt(outFileName, features, delimiter=',',newline='\n', header="Ts,Td,B25,B50,B75,c6,c5,c4,c3,c2,c1,c0,Pd,Ps,L3,L4")
