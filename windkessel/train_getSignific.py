from pandas import read_csv, DataFrame
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression, SGDRegressor
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.cross_validation import train_test_split
import pickle
import matplotlib.pyplot as plt
import xgboost as xgb

dataset = read_csv('prepareDataTrain.csv',',')
print dataset.head()
print dataset.corr()
#dataset = dataset.drop(['L3','L4'], axis=1)
#dataset = dataset.drop(['c6','c5','c4','c3','c2','c1','c0'], axis=1)
print dataset.head()

dataset = dataset.drop(['L3','L4'], axis=1)
trgPd = dataset['Pd']
trgPs = dataset['Ps']
trn = dataset.drop(['Pd','Ps'], axis=1)

#dataset = dataset.drop(['Pd','Ps'], axis=1)
#trgL3 = dataset['L3']
#trgL4 = dataset['L4']
#trn = dataset.drop(['L3','L4'], axis=1)


#models = [
#LinearRegression(), #Ordinary Least Squares
#RandomForestRegressor(n_estimators=100, max_features ='sqrt'), #RandomForest
#KNeighborsRegressor(n_neighbors=6),
#SVR(kernel='linear'), # linear SVM
#LogisticRegression() #LogisticRegression
#SGDRegressor(loss='huber',n_iter=10000)
#]

Xtrn, Xtest, Ytrn, Ytest = train_test_split(trn, trgPs, test_size=0.4)

#make tmp struct
TestModels = pd.DataFrame()
tmp = {}
print Ytrn
i = 8

gbm = xgb.XGBClassifier(silent=False, nthread=4, max_depth=10, n_estimators=800, subsample=0.5, learning_rate=0.03, seed=1337)
gbm.fit(Xtrn, Ytrn)
bst = gbm.booster()
imps = bst.get_fscore()
print imps
	
#	#write data and result DataFrame
	#TestModels = TestModels.append([tmp])
#make index for model's name
#TestModels.set_index('Model', inplace=True)
#print 'h'

#TestModels.set_index('Model', inplace=True)
#fig, axes = plt.subplots(ncols=1, figsize=(10,4))
#TestModels.R2_Y1.plot(ax=axes[0], kind='bar', title='R2_Y1')
#TestModels.R2_Y1.plot(ax=axes, kind='bar', color='green', title='R2_Y2')

#plt.show()

