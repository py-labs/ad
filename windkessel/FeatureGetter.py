#-*- coding: utf-8 -*-



from numpy import *
from matplotlib.pyplot import *
from matplotlib.pyplot import plot, scatter, show
from scipy import signal
import scipy
import numpy as np
import warnings

class CycleData:
    def __init__(self, x, y, Pd, Ps, xInMax):
        self.x = x
        self.y = y
        self.Pd = Pd
        self.Ps = Ps
	self.xInMax = xInMax


class Feature:
	def __init__(self, cycle, isTest):
		self.cycle = cycle
		self.poly_rate = 6
		self.isTest = isTest
	def find_nearest(self, array, value):
		idx = (np.abs(array-value)).argmin()
		return idx
	def get(self):
        	with warnings.catch_warnings(): 
			warnings.filterwarnings('error')
			try:
				Ts = self.cycle.x[self.cycle.xInMax] - self.cycle.x[0]
				Td = self.cycle.x[len(self.cycle.x)-1] - self.cycle.x[self.cycle.xInMax]
				Bs = []
				height = self.cycle.y[self.cycle.xInMax]
				heightCoefs = [0.25*height, 0.5*height, 0.75*height]
				y1 = self.cycle.y[0:self.cycle.xInMax]
				y2 = self.cycle.y[self.cycle.xInMax:len(self.cycle.y)]
		
				for h in heightCoefs:
					idx1 = self.find_nearest(y1, h)
					idxs = []
					idxs.append(idx1)
					idx2 = self.find_nearest(y2, h)
					idxs.append(idx2 + self.cycle.xInMax)
					Bs.append(self.cycle.x[idxs[1]] - self.cycle.x[idxs[0]])
				
				x_part0 =  np.subtract(self.cycle.x, self.cycle.x[0])
				coefficients = polyfit(x_part0, self.cycle.y, self.poly_rate)
				
				
				L3 = -1
				L4 = -1
				if (not self.isTest):
					L4 = -Td/log(self.cycle.Pd/self.cycle.Ps)
					K1 = self.cycle.Ps - self.cycle.Pd*np.e**(-Ts/L4)
					K2 = K1 / (1+e**(-Ts/L4))
					K3 = K2*(Ts**2 + L4**2 * np.pi**2)
					L3 = K3 / (Ts *np.pi*L4)
			
				feature = []
				feature.append(Ts)	
				feature.append(Td)
				for bs in Bs:
					feature.append(bs)
				for c in coefficients:
					feature.append(c)
				feature.append(self.cycle.Pd)
				feature.append(self.cycle.Ps)
				feature.append(L3)
				feature.append(L4)
			        
				return feature
			except np.RankWarning:
		        	print "no enouth data"
				return []
        

class FeatureGetter:
	def __init__(self, PPG, Pd, Ps, xStepMs, peaksThr, isTest):
		self.PPG = PPG
		self.Pd = Pd
		self.Ps = Ps
		self.peaksThr = peaksThr
		self.xStepMs = xStepMs
		self.isTest = isTest
	
	def peakdet(self, v, delta, x = None):
		"""
		Converted from MATLAB script at http://billauer.co.il/peakdet.html
		
		Returns two arrays
		
		function [maxtab, mintab]=peakdet(v, delta, x)
		%PEAKDET Detect peaks in a vector
		%        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
		%        maxima and minima ("peaks") in the vector V.
		%        MAXTAB and MINTAB consists of two columns. Column 1
		%        contains indices in V, and column 2 the found values.
		%      
		%        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
		%        in MAXTAB and MINTAB are replaced with the corresponding
		%        X-values.
		%
		%        A point is considered a maximum peak if it has the maximal
		%        value, and was preceded (to the left) by a value lower by
		%        DELTA.
		
		% Eli Billauer, 3.4.05 (Explicitly not copyrighted).
		% This function is released to the public domain; Any use is allowed.
		    
		"""
		maxtab = []
		mintab = []
		
		if x is None:
			x = arange(len(v))

		v = asarray(v)
		
		if len(v) != len(x):
			sys.exit('Input vectors v and x must have same length')
		
		if not isscalar(delta):
			sys.exit('Input argument delta must be a scalar')
		
		if delta <= 0:
			sys.exit('Input argument delta must be positive')
		
		mn, mx = Inf, -Inf
		mnpos, mxpos = NaN, NaN
		
		lookformax = True
		for i in arange(len(v)):
			this = v[i]
			
			if this > mx:
				mx = this
				mxpos = x[i]
			if this < mn:
				mn = this
				mnpos = x[i]
			if lookformax:

				if this < mx-delta:
					maxtab.append((mxpos, mx))
					mn = this
					mnpos = x[i]
					lookformax = False
			else:
				if this > mn+delta:
					mintab.append((mnpos, mn))
					mx = this
					mxpos = x[i]
					lookformax = True
		return array(maxtab), array(mintab)

	def getXTimeMs(self,x, xStepMs):
		xTimeMs = []
		for i in x:
			x_new = i*xStepMs
			xTimeMs.append(x_new)	
		return xTimeMs

	def divideHeartCycles(self):
		x = np.arange(len(self.PPG))
		y = self.PPG
		print len(y)
		maxtab, mintab = self.peakdet(y, self.peaksThr)
		xTimeMs = self.getXTimeMs(x, self.xStepMs)
		heartCyclesData = []
		for i in range(len(mintab)-2):

			x_part = xTimeMs[int(mintab[i+1][0]):int(mintab[i+2][0])]
			xInMax = x[int(maxtab[i+2][0])] - x[int(mintab[i+1][0])]
			
			y_part = y[int(mintab[i+1][0]):int(mintab[i+2][0])]
			if (not self.isTest):
				Pd_part = self.Pd[int(mintab[i+1][0]):int(mintab[i+2][0])]
				
				Ps_part = self.Ps[int(mintab[i+1][0]):int(mintab[i+2][0])]
			        
				if (np.amax(Pd_part) == np.amin(Pd_part) and np.amax(Ps_part) == np.amin(Ps_part) and len(Pd_part) == len(Ps_part) and len(Ps_part) != 0):
					cycleData = CycleData(x_part, y_part, np.amax(Pd_part), np.amax(Ps_part), xInMax)
					heartCyclesData.append(cycleData)			
			else:
				cycleData = CycleData(x_part, y_part, -1, -1, xInMax)
				heartCyclesData.append(cycleData)				

		
		#plot(x, y, color='blue')
		#scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
		#scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
		#show()

		#plot(xTimeMs, y, color='blue')
		#show()
		print 'f'		
		return heartCyclesData
	
	def getFeatures(self):
		heartCyclesData = self.divideHeartCycles()
		#print heartCyclesData[0].Ps
		features = []
		for cycle in heartCyclesData:
			feature = Feature(cycle, self.isTest).get()
			if (len(feature) != 0):
				features.append(feature)		

		#print heartCyclesData[0].Ps
		return features
				
