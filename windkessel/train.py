from pandas import read_csv, DataFrame
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.cross_validation import train_test_split
import pickle
import matplotlib.pyplot as plt

dataset = read_csv('prepareDataTrain.csv',',')
print dataset.head()
print dataset.corr()
dataset = dataset.drop(['L3','L4'], axis=1)
#dataset = dataset.drop(['c6','c5','c4','c3','c2','c1','c0'], axis=1)
print dataset.head()

trgPd = dataset['Pd']
trgPs = dataset['Ps']
trn = dataset.drop(['Pd','Ps'], axis=1)


models = [
#LinearRegression(), #Ordinary Least Squares
#RandomForestRegressor(n_estimators=100, max_features ='sqrt'), #RandomForest
#KNeighborsRegressor(n_neighbors=6),
SVR(kernel='linear'), # linear SVM
LogisticRegression() #LogisticRegression
]

Xtrn, Xtest, Ytrn, Ytest = train_test_split(trn, trgPs, test_size=0.4)

#make tmp struct
TestModels = pd.DataFrame()
tmp = {}
print Ytrn
i = 3
for model0 in models:
	#get name of model
	i += 1
	m = str(model0)
	print m
	tmp['Model'] = m[:m.index('(')]    
	model0.fit(Xtrn, Ytrn)
	filename = 'finalized_modelPs0' + str(i) + '.sav'
	pickle.dump(model0, open(filename, 'wb'))
	#model = pickle.load(open(filename, 'rb'))
	tmp['R2_Y1'] = r2_score(Ytest, model0.predict(Xtest))
	TestModels = TestModels.append([tmp])
	#for every cols result set
	#for i in  xrange(Ytrn.shape[1]):
		#print 'g'
		#train model
		#print Xtrn
		#print i
		#a = Ytrn.iloc[i]
		#print a
		#print Ytrn[:,i]
		#model.fit(Xtrn.to_frame(), Ytrn.iloc[i].to_frame()) 
		#get determination coeff
		#tmp['R2_Y%s'%str(i+1)] = r2_score(Ytest.iloc[0], model.predict(Xtest))
#	#write data and result DataFrame
	#TestModels = TestModels.append([tmp])
#make index for model's name
#TestModels.set_index('Model', inplace=True)
#print 'h'

TestModels.set_index('Model', inplace=True)
fig, axes = plt.subplots(ncols=1, figsize=(10,4))
#TestModels.R2_Y1.plot(ax=axes[0], kind='bar', title='R2_Y1')
TestModels.R2_Y1.plot(ax=axes, kind='bar', color='green', title='R2_Y2')

plt.show()
