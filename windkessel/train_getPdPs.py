from pandas import read_csv, DataFrame
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.cross_validation import train_test_split
import pickle
import matplotlib.pyplot as plt


dataset = read_csv('prepareDataTrain.csv',',')
print dataset.head()
print dataset.corr()
dataset = dataset.drop(['L3','L4'], axis=1)
#dataset = dataset.drop(['c6','c5','c4','c3','c2','c1','c0'], axis=1)
print dataset.head()

trgPd = dataset['Pd']
trgPs = dataset['Ps']

n = 150

Pds = [ i for i in range(n)]

print Pds
countPds = [0]*n

print len(Pds)
print len(countPds)
for pd in trgPd:
	#print pd
	countPds[int(pd)] += 1

print countPds

