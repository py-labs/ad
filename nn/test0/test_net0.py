import pickle
import thinkdsp
import numpy as np
import math

import matplotlib.pyplot as plt
import numpy as np


def show(net):
    xs = np.linspace(0, 9, num=100)
    output = [net.activate([x])[0] for x in xs]
    hidden = [[] for i in range(net['hidden0'].dim)]
    for x in xs:
        net.activate([x])
        for i in range(net['hidden0'].dim):
            hidden[i].append(net['hidden0'].outputbuffer[
                             net['hidden0'].offset][i])

    fig = plt.figure(figsize=(8, 4), dpi=80)

    fig.add_subplot(121)
    plt.grid()
    plt.axis([0, 9, 0, 9])
    plt.plot(xs, output)
    plt.title('Output')
    plt.xlabel('x')

    fig.add_subplot(122)
    plt.grid()
    plt.margins(0.0, 0.1)
    for i in range(len(hidden)):
        plt.plot(xs, hidden[i])
    plt.title('Hidden')
    plt.xlabel('x')

    plt.tight_layout()
    plt.show()


def format(net):
    r = ''

    test_input = [
        1, 2, 3, 4, 5, 6, 7, 8,
        -1, 0, 0.5, 4.1, 4.5, 4.9, 8.5, 9
    ]

    for i in test_input:
        r += '{} & {:.5f} \\\\\n'.format(i, net.activate([i])[0])

    return r

def pesos_conexiones(n):
    for mod in n.modules:
        for conn in n.connections[mod]:
            print conn
            for cc in range(len(conn.params)):
                print conn.whichBuffers(cc), conn.params[cc]

fileObject = open('trained_net_01_900_0001_5000','r')
net = pickle.load(fileObject)

#net.draw()
#from pylab import plot, show
#plot(net)
#show()
#net.useGraph()

pesos_conexiones(net)
print net

print format(net)
show(net)