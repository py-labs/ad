import thinkdsp
import numpy as np
import math

height = 1
width = 900
Fs=100


def estimate(data):
    length=len(data)
    wave=thinkdsp.Wave(ys=data,framerate=Fs)
    spectrum=wave.make_spectrum()
    spectrum_heart=wave.make_spectrum()
    spectrum_resp=wave.make_spectrum()

    fft_mag=list(np.absolute(spectrum.hs))
    fft_length= len(fft_mag)

    spectrum_heart.high_pass(cutoff=0.5,factor=0)
    spectrum_heart.low_pass(cutoff=4,factor=0)
    fft_heart=list(np.absolute(spectrum_heart.hs))

    max_fft_heart=max(fft_heart)
    heart_sample=fft_heart.index(max_fft_heart)
    hr=heart_sample*Fs/length*60

    spectrum_resp.high_pass(cutoff=0.15,factor=0)
    spectrum_resp.low_pass(cutoff=0.5,factor=0)
    fft_resp=list(np.absolute(spectrum_resp.hs))

    max_fft_resp=max(fft_resp)
    resp_sample=fft_resp.index(max_fft_resp)
    rr=resp_sample*Fs/length*60


    if hr<10:
        return hr, 0
    else:
        return hr,rr


def getBP(ppg, scaleFactor = 1):
	htbt, rrt = estimate(ppg)
	y = []
	for p in ppg:
		y.append(p*scaleFactor)
	L = 10
	j = 1
	T = 0.24
	N = len(y)
	
	for cntr in range(1,N):
		suom=0
		acsum=0
	print "cntr: "
	print cntr
	for cntr in range(1,N):
		suom = suom + y[cntr-1]
		acsum = acsum + y[cntr-1]*y[cntr-1]
	print "suom: " + str(suom)
	print "acsum: " + str(acsum)
	avg = suom/N
	print 'avg: ' + str(avg) 
	#ac = math.sqrt(acsum/N)
	#print 'ac: ' + str(ac)
	print "y: " + str(y)
	for cntr in range(1,N):
		y[cntr - 1] = y[cntr - 1] - avg
	print "y: " + str(y) 
	d = np.empty(N-L)
	print "d: " + str(d)
	j=1
	for cntr in range(L,N):
		d[j - 1] = y[cntr]
		j = j + 1
	print "d: " + str(d)
	#formation of D
	D = np.empty([N - L, L])
	#print "D: " + str(D)
	r = 1
	c = 1
	for cntr in range(1, N - L + 1):
		for cntr1 in range(1, L + 1):
			D[r - 1, c - 1] = y[L - cntr1 + cntr - 1]
			c = c + 1
		c = 1
		r = r + 1
	print "D: " + str(D)	
	inversa = np.linalg.pinv(D)
	print 'inversa: ' + str(inversa)
	a = np.empty(L)
	print 'a: ' + str(a)
	a = np.dot(inversa, d)
	print 'a: ' + str(a)
	#obtaining z roots
	z = np.empty(11)
	print 'z: ' + str(z) 
	for cntr in range(0, 11):
		if cntr == 0:
			z[cntr] = 1
		else:
			z[cntr] = -a[cntr - 1]
	print 'z: ' + str(z)
	rots = np.roots(z)
	print 'rots: ' + str(rots)

	#formation of y=uc and y=y1
	y1 = np.empty(L)
	for cntr in range(0,L):
		y1[cntr] = y[L + cntr - 1]
	print 'y1:' + str(y1)

	u = np.empty([L, L], complex)
	c1 = np.empty(L)
	
	r = 1
	c = 1
	for cntr in range(1, L + 1):
		for cntr1 in range(1, L + 1):
			u[r - 1, c - 1] = u[r - 1, c - 1] + 0j
			u[r - 1, c - 1] = pow(rots[cntr1 - 1], r - 1)
			c = c+1
		c = 1
		r = r+1
	print 'u: ' + str(u)
	print 'c1:' + str(c1 )
	c1 = np.dot(np.linalg.pinv(u), y1)
	print 'c1:' + str(c1 )
	#amplitude
	val = np.empty(L)
	for cntr in range(0, L-1):
		val[cntr] = abs(c1[cntr]*2)
	print 'val:' + str(val)
	#frequency
	f = np.empty(L)
	for cntr in range(0, L-1):
		re = rots[cntr].real
		im = rots[cntr].imag
		kk = 2*3.1416*T
		if re != 0:
			f[cntr] = math.atan(im/re)/kk
	
	
	#phi = np.empty(L)
	#for cntr in range(0,L):
	#	# print c1[cntr].real , c1[cntr].imag
	#	if c1[cntr].real == 0:
	#		phi[cntr] = cmath.pi/2
	#	else:
	#		real = c1[cntr].imag
	#		imaginary = c1[cntr].real
	#		if real != 0:
	#			phi[cntr] = math.atan(imaginary/real)
	#		else:
	#			phi[cntr] = 0
	#			val[cntr] = 0
	#			f[cntr] = 0
	for cntr in range(0,L):
		if f[cntr] < 0.2 or val[cntr] < 0.01:
			val[cntr] = 0
			#phi[cntr] = 0
			f[cntr] = 0
	print 'f: ' + str(f)
	print 'val: ' + str(val)
	print 'zip(f, val): ' + str(zip(f, val))
	print 'sorted zip(f, val): ' + str(sorted(zip(f, val)))
	
	#print 'sorted zip(f, phi): ' + str(sorted(zip(f, phi)))


	#val = [x for (y,x) in sorted(zip(f, val))]
	#phi = [x for (y,x) in sorted(zip(f, phi))]
	#print 'val: ' + str(val)
	#print 'phi: ' + str(phi)
	
	f = sorted(f)
	print 'f: ' + str(f) 
	
	pout=[f[9],f[8],f[7],f[6],f[5],val[9],val[8],val[7],val[6],val[5],avg]
	#print "pout: " + str(pout) 
	#bp1 = str(self.net.activate(pout)).split()
	#print bp1
	#bp1_dia = str(bp1[1])[:5]
	#bp1_sys = str(bp1[2])[:5]
	#print "Blood Pressure [normal diastolic < 80, systolic < 140] : " + str(bp1[1]) + " " + str(bp1[2])
	
	#return bp1_sys, bp1_dia
	return 0, 0


ppg = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]

bp1, bp2 = getBP(ppg, 1.0)