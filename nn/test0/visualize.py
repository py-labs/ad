import matplotlib.pyplot as plt
import numpy as np


def show(net):
    xs = np.linspace(0, 9, num=100)
    output = [net.activate([x])[0] for x in xs]
    hidden = [[] for i in range(net['hidden0'].dim)]
    for x in xs:
        net.activate([x])
        for i in range(net['hidden0'].dim):
            hidden[i].append(net['hidden0'].outputbuffer[
                             net['hidden0'].offset][i])

    fig = plt.figure(figsize=(8, 4), dpi=80)

    fig.add_subplot(121)
    plt.grid()
    plt.axis([0, 9, 0, 9])
    plt.plot(xs, output)
    plt.title('Output')
    plt.xlabel('x')

    fig.add_subplot(122)
    plt.grid()
    plt.margins(0.0, 0.1)
    for i in range(len(hidden)):
        plt.plot(xs, hidden[i])
    plt.title('Hidden')
    plt.xlabel('x')

    plt.tight_layout()
    plt.show()
