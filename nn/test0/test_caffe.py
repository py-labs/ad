
import sys
import os
import dlib
import glob
import cv2

import FR_CAFFE

image_path = '10156_65_119.png'
#image_path = '10197_48_98.png'
img0 = cv2.imread(image_path, 1)
img = cv2.resize(img0, (256,256))
fr = FR_CAFFE.FR_CAFFE('./spec_C_deploy_reg0.prototxt', './spec_regression_iter_152010.caffemodel', 'fc8', False, 0)
templ3 = fr.getTemplate(img)
print templ3