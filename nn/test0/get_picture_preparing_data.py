import numpy as np
import os
import sys
import argparse
import glob
import time
import csv
#import caffe
import matplotlib.pyplot as plt
import serial
from array import array
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab
import pylab as pl
import math
import cmath
from multiprocessing import Process
import thinkdsp
import adaptfilt as adf
from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import pickle
import scipy as sp
import pandas as pd
import csv as csv
from sklearn.ensemble import RandomForestClassifier
import os
import json
import requests
from collections import OrderedDict
from sklearn.cross_validation import train_test_split
from utils import *

# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 1500
Fs=100

def save_picture(signal, frame_rate, fileName):

    """A spectrogram, or sonogram, is a visual representation of the spectrum
    of frequencies in a sound.  Horizontal axis represents time, Vertical axis
    represents frequency, and color represents amplitude.
    """

    size = (3, 3)     # inches
    dots = 300          # dpi

    #sound_array, frame_rate = get_wave_info(wav_file)
    sound_array = signal
    fig = plt.figure(num=None, figsize=size, dpi=dots, frameon=False)
    #plt.title('specgram ')
    pss, freqs, t = mlab.specgram(sound_array, Fs=frame_rate)

    ext = [0, t.max() / 10000, 0, freqs.max() / 1000]
    log_pss = np.log(pss)
    #plt.imshow(log_pss, origin='lower', cmap='jet', aspect='auto', extent=ext)
    #show()
    #plt.axis('off')
    #ax = plt.Axes(fig, [0., 0., 1., 1.])
    #ax.set_axis_off()
    #fig.add_axes(ax)

    #plt.savefig('spectrogram.png', bbox_inches='tight', pad_inches=0)
    
    #fig = plt.figure(frameon=False)
    #fig.set_size_inches(1,1)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(log_pss, aspect='auto')
    fig.savefig(fileName)
    plt.clf()
    plt.cla()
    plt.close()

def process_data_theirs(data_path):
    sample_list_x = [] # List of lists

    current_sample_x = []

    label_list1  = []
    label_list2  = []
    dict = {}
    with open(data_path, 'rt') as f:
        reader = csv.reader(f)
        prev_label1 = " "
        prev_label2 = " "
        for row in reader:
            if not row:
                continue
            label1 = int(float(row[0]))
            label2 = int(float(row[1]))
            x = float(row[2])

            # Reset when new label is found
            if (prev_label1 != label1 or prev_label2 != label2):
                prev_label1 = label1
                prev_label2 = label2
                current_sample_x = []


            current_sample_x.append(x)

            # Store when we have the length
            if (len(current_sample_x) == width):
		#print current_sample_x
		
                xvu = [label1, label2]
		
		if str(xvu) in dict:
			dict[str(xvu)] += 1
		else:
			dict[str(xvu)] = 0		
		if (dict[str(xvu)] < 35):
			#params = calc_parameter(current_sample_x)		
			cleanedSignal = medianFilter(current_sample_x)
			sample_list_x.append(cleanedSignal)
        	        label_list1.append(label1)
                	label_list2.append(label2)

                # Retain only certain overlap percentages of the sample
                current_sample_x = current_sample_x[int(len(current_sample_x) * (1.0-percent_overlap)):]

    return sample_list_x, label_list1, label_list2


def get_data_treirs(sample_list_x, label_list1, label_list2):
	
    path = "E:/CARS/PRESSURE/train_image/"	
    for i in range(len(sample_list_x)):
	#if (i > 1074):
	path_end = path + str(i) + "_" + str(label_list1[i]) + "_" + str(label_list2[i]) + ".png"
	print path_end
	save_picture(sample_list_x[i], Fs, path_end)
		 


def main():
    data_path = 'E:/CARS/PRESSURE/ad/cnn/prepareDataTest_all00.csv'

    sample_list_x, label_lis1, label_list2 = process_data_theirs(data_path)
    get_data_treirs(sample_list_x, label_lis1, label_list2)

if __name__ == '__main__':
    main()