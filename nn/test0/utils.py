import matplotlib.pyplot as plt

#------------------------------------------------------------------------------
#        Mean Filter on feature
#------------------------------------------------------------------------------
def meanFilter(signal,windowSize=5) :
    totalValuesNumber=len(signal)
    cleanedSignal=[]
    lesserI=-(windowSize/2)
    upperI=min((windowSize+1)/2,len(signal))
    sumOfWindow=sum(signal[0:upperI])
    realSize=upperI
    
    for i in range(totalValuesNumber) :
        cleanedSignal.append(sumOfWindow/realSize)
        lesserI+=1
        upperI+=1
        if (lesserI>0) :
            sumOfWindow-=signal[lesserI-1]
            realSize-=1
        if (upperI<=totalValuesNumber) :
            sumOfWindow+=signal[upperI-1]
            realSize+=1
    return cleanedSignal

def medianFilter(signal,windowSize=5) :
    totalValuesNumber=len(signal)
    cleanedSignal=[]
    lesserI=-(windowSize/2)
    upperI=min((windowSize+1)/2,len(signal))

    window=signal[0:upperI]
    realSize=upperI
    
    for i in range(totalValuesNumber) :
        sortedWindow=sorted(window)
        cleanedSignal.append(sortedWindow[realSize/2])
        lesserI+=1
        upperI+=1
        if (lesserI>0) :
            window.pop(0)
            realSize-=1
        if (upperI<=totalValuesNumber) :
            window.append(signal[upperI-1])
            realSize+=1
    return cleanedSignal


#------------------------------------------------------------------------------
#        Plot Mean Filter on feature
#------------------------------------------------------------------------------
def showSignalMeanAndMedianFiltering(signal,WindowSize=60) :
    meanSignal=meanFilter(signal,WindowSize)
    medianSignal=medianFilter(signal,WindowSize)
    plt.figure(1)
    plt.clf()
    plt.subplot(311)
    plt.title('Duree du signal : {0}s'.format(len(signal)))
    plt.plot(range(len(signal)),signal, '-b')
    plt.xlabel("t (s)")
    plt.ylabel("signal")
    plt.subplot(312)
    plt.plot(range(len(meanSignal)),meanSignal, '-g')
    plt.ylabel("mean filter on signal")
    plt.subplot(313)
    plt.plot(range(len(medianSignal)),medianSignal, '-r')
    plt.ylabel("median filter on signal")
    plt.show()
#------------------------------------------------------------------------------

def peakdet(v, delta, x = None):
	maxtab = []
	mintab = []
	   
	if x is None:
		x = arange(len(v))
	
	v = asarray(v)
	
	if len(v) != len(x):
		sys.exit('Input vectors v and x must have same length')
	
	if not isscalar(delta):
		sys.exit('Input argument delta must be a scalar')
	
	if delta <= 0:
		sys.exit('Input argument delta must be positive')
	
	mn, mx = Inf, -Inf
	mnpos, mxpos = NaN, NaN
	
	lookformax = True
	
	for i in arange(len(v)):
		this = v[i]
		if this > mx:
			mx = this
			mxpos = x[i]
		if this < mn:
			mn = this
			mnpos = x[i]
	    	
		if lookformax:
			if this < mx-delta:
				maxtab.append((mxpos, mx))
				mn = this
				mnpos = x[i]
				lookformax = False
		else:
			if this > mn+delta:
				mintab.append((mnpos, mn))
				mx = this
				mxpos = x[i]
				lookformax = True
        return array(maxtab), array(mintab)

