from pyAudioAnalysis import audioBasicIO as io, audioFeatureExtraction as aF, audioSegmentation


def mtCNN_classification(signal, Fs, mtWin, mtStep, RGB_singleFrame_net, SOUND_mean_RGB, transformer_RGB, classNamesCNN):
    mtWin2 = int(mtWin * Fs)
    mtStep2 = int(mtStep * Fs)
    stWin = 0.020
    stStep = 0.015    
    N = len(signal)
    curPos = 0
    count = 0
    fileNames = []
    flagsInd = []
    Ps = []
    randomString = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(5)))
    while (curPos < N):                 # for each mid-term segment
        N1 = curPos
        N2 = curPos + mtWin2 + stStep*Fs
        if N2 > N:
            N2 = N
        xtemp = signal[int(N1):int(N2)]                # get mid-term segment        

        specgram, TimeAxis, FreqAxis = aF.stSpectogram(xtemp, Fs, round(Fs * stWin), round(Fs * stStep), False)     # compute spectrogram
        if specgram.shape[0] != specgram.shape[1]:                                                                  # TODO (this must be dynamic!)
            break
        specgram = scipy.misc.imresize(specgram, float(227.0) / float(specgram.shape[0]), interp='bilinear')        # resize to 227 x 227
        
        imSpec = Image.fromarray(np.uint8(matplotlib.cm.jet(specgram)*255))                                         # create image
        curFileName = randomString + "temp_{0:d}.png".format(count)
        fileNames.append(curFileName)    
        scipy.misc.imsave(curFileName, imSpec)
        
        T1 = time.time()
        #output_classes, outputP = singleFrame_classify_video(curFileName, RGB_singleFrame_net, transformer_RGB, False, classNamesCNN)        
        #T2 = time.time()
        #print T2 - T1
        #flagsInd.append(classNamesCNN.index(output_classes[0]))        
        #Ps.append(outputP[0])
        #print flagsInd[-1]
        curPos += mtStep2               
        count += 1              
    return 0#np.array(flagsInd), classNamesCNN, np.array(Ps)


name = '1.wav'
WIDTH_SEC = 2.4  

[Fs, x] = io.readAudioFile(fileName)  

x = io.stereo2mono(x)

#[flagsInd, classesAll, P] = mtCNN_classification(x, Fs, WIDTH_SEC, 1.0, RGB_singleFrame_net, SOUND_mean_RGB, transformer_RGB, classNamesCNN)    #  apply the CNN mid-term classifier   
t = mtCNN_classification(x, Fs, WIDTH_SEC, 1.0, RGB_singleFrame_net, SOUND_mean_RGB, transformer_RGB, classNamesCNN)    #  apply the CNN mid-term classifier   