def format(net):
    r = ''

    test_input = [
        1, 2, 3, 4, 5, 6, 7, 8,
        -1, 0, 0.5, 4.1, 4.5, 4.9, 8.5, 9
    ]

    for i in test_input:
        r += '{} & {:.5f} \\\\\n'.format(i, net.activate([i])[0])

    return r