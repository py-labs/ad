import TextBaseReader
#import BpGetter
import numpy as np
import thinkdsp
import math
import cmath
import pickle
from matplotlib.pyplot import plot, scatter, show
import matplotlib.mlab as mlab
from python_speech_features import mfcc
from utils import *
#from features import *

def graph_spectogram(signal, frame_rate):
    """A spectrogram, or sonogram, is a visual representation of the spectrum
    of frequencies in a sound.  Horizontal axis represents time, Vertical axis
    represents frequency, and color represents amplitude.
    """

    size = (3, 3)     # inches
    dots = 300          # dpi

    #sound_array, frame_rate = get_wave_info(wav_file)
    sound_array = signal
    fig = plt.figure(num=None, figsize=size, dpi=dots, frameon=False)
    #plt.title('specgram ')
    pss, freqs, t = mlab.specgram(sound_array, Fs=frame_rate)

    ext = [0, t.max() / 10000, 0, freqs.max() / 1000]
    log_pss = np.log(pss)
    #plt.imshow(log_pss, origin='lower', cmap='jet', aspect='auto', extent=ext)
    #show()
    #plt.axis('off')
    #ax = plt.Axes(fig, [0., 0., 1., 1.])
    #ax.set_axis_off()
    #fig.add_axes(ax)

    #plt.savefig('spectrogram.png', bbox_inches='tight', pad_inches=0)
    
    #fig = plt.figure(frameon=False)
    #fig.set_size_inches(1,1)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(log_pss, aspect='auto')
    fig.savefig('spectrogram.png')

def plot_spectrogramm(G, **kwargs):
    r"""
    Plot the spectrogramm of the given graph.
    Parameters
    ----------
    G : Graph object
        Graph to analyse.
    node_idx : ndarray
        Order to sort the nodes in the spectrogramm
    Example
    --------
    >>> import numpy as np
    >>> from pygsp import graphs, plotting
    >>> G = graphs.Ring(15)
    >>> plotting.plot_spectrogramm(G)
    """
    global window_list
    from pygsp.features import compute_spectrogramm
    if 'window_list' not in globals():
        window_list = {}

    if not qtg_import:
        raise NotImplementedError("You need pyqtgraph to plot the spectrogramm at the moment. Please install dependency and retry.")

    if not hasattr(G, 'spectr'):
        compute_spectrogramm(G)

    M = G.spectr.shape[1]
    node_idx = kwargs.pop('node_idx', None)
    spectr = np.ravel(G.spectr[node_idx, :] if node_idx is not None else G.spectr)
    min_spec, max_spec = np.min(spectr), np.max(spectr)

    pos = np.array([0., 0.25, 0.5, 0.75, 1.])
    color = np.array([[20, 133, 212, 255], [53, 42, 135, 255], [48, 174, 170, 255],
                     [210, 184, 87, 255], [249, 251, 14, 255]], dtype=np.ubyte)
    cmap = pg.ColorMap(pos, color)

    w = pg.GraphicsWindow()
    w.setWindowTitle("Spectrogramm of {}".format(G.gtype))
    v = w.addPlot(labels={'bottom': 'nodes',
                          'left': 'frequencies {}:{:.2f}:{:.2f}'.format(0, G.lmax/M, G.lmax)})
    v.setAspectLocked()

    spi = pg.ScatterPlotItem(np.repeat(np.arange(G.N), M), np.ravel(np.tile(np.arange(M), (1, G.N))), pxMode=False, symbol='s',
                             size=1, brush=cmap.map((spectr.astype(float) - min_spec)/(max_spec - min_spec), 'qcolor'))
    v.addItem(spi)

    window_list[str(uuid.uuid4())] = w

path = "E:/CARS/PRESSURE/test_data/2017-01-30-10-27-29_1.csv"
#path = "E:/CARS/PRESSURE/Pulse-out/2017-03-17-11-04-05_2.csv"
#path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test15/uq_vsd_case31_fulldata_11.csv"
textBaseReader = TextBaseReader.TextBaseReader(path)
PPG = textBaseReader.getPPG(0.1)
Fs = 100
#wave = thinkdsp.Wave(ys = PPG,framerate = Fs)
#data = np.transpose(PPG)
#mfcc_feature = mfcc(data, Fs)
#print len(data)
#print len(mfcc_feature)
#plot(PPG)
#scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
#scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
#show()

#showSignalMeanAndMedianFiltering(PPG)
#wf = WaveletFeatures.WaveletFeatures(PPG)
#feature = wf.get_features()

cleanedSignal = medianFilter(PPG)
#print cleanedSignal
#wave = thinkdsp.Wave(ys = data,framerate = Fs)
#spectrum = wave.make_spectrum()
y = x.astype(np.float)
maxtab, mintab = peakdet(y, 50)

graph_spectogram(cleanedSignal, Fs)
#plot_spectrogramm(cleanedSignal)

