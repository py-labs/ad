import numpy as np
from numpy import NaN, Inf, arange, isscalar, asarray, array
from matplotlib.pyplot import plot, scatter, show
import math


def peakdet(v, delta, x = None):
	maxtab = []
	mintab = []
	   
	if x is None:
		x = arange(len(v))
	
	v = asarray(v)
	
	if len(v) != len(x):
		sys.exit('Input vectors v and x must have same length')
	
	if not isscalar(delta):
		sys.exit('Input argument delta must be a scalar')
	
	if delta <= 0:
		sys.exit('Input argument delta must be positive')
	
	mn, mx = Inf, -Inf
	mnpos, mxpos = NaN, NaN
	
	lookformax = True
	
	for i in arange(len(v)):
		this = v[i]
		if this > mx:
			mx = this
			mxpos = x[i]
		if this < mn:
			mn = this
			mnpos = x[i]
	    	
		if lookformax:
			if this < mx-delta:
				maxtab.append((mxpos, mx))
				mn = this
				mnpos = x[i]
				lookformax = False
		else:
			if this > mn+delta:
				mintab.append((mnpos, mn))
				mx = this
				mxpos = x[i]
				lookformax = True

	return array(maxtab), array(mintab)



	
def main():
	data_path = 'E:/CARS/PRESSURE/data_estimate/txt/abp1_0.txt'
	data_path_out = 'E:/CARS/PRESSURE/data_estimate/txt/bp1_0.txt'

	f = open(data_path, 'r')
	f_out = open(data_path_out,'w') 
	for line in f:
		line = line.rstrip('\t\n')
		line = line.rstrip('\n')
		strs = line.split("\t")
		#print strs
		x = np.array(strs, dtype='|S4')
		y = x.astype(np.float)
		maxtab, mintab = peakdet(y, 50)
		
    		
		countMax = len(maxtab)
		countMin = len(mintab)
		
		
		countMaxVal = 0
		sumMaxVal = 0
		
		countMinVal = 0
		sumMinVal = 0


		for i in range(min(countMax, countMin)):
			countMaxVal += 1
			sumMaxVal += maxtab[i][1]
			
			countMinVal += 1
			sumMinVal += mintab[i][1]
		
		if (countMaxVal != 0 and countMinVal != 0):
			
			res_min =  (float)(sumMinVal)/countMinVal
			res_max =  (float)(sumMaxVal)/countMaxVal
	
			
			sout = str(res_min) + '\t' + str(res_max) + '\n'
			f_out.write(sout)
		else:
			
			res_min =  0
			res_max =  0
			sout = str(res_min) + '\t' + str(res_max) + '\n'
			f_out.write(sout)

		#plot(y)
		#scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
		#scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
    		#show()

	f.close()
	f_out.close()
if __name__ == '__main__':
    main()