import numpy as np
import os
import sys
import argparse
import glob
import time
import csv
import caffe
import matplotlib.pyplot as plt
import serial
from array import array
from matplotlib import pyplot as plt
import pylab as pl
import math
import cmath
from multiprocessing import Process
import thinkdsp
import adaptfilt as adf
from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import pickle
import scipy as sp
import pandas as pd
import csv as csv
from sklearn.ensemble import RandomForestClassifier
import os
import json
import requests
from collections import OrderedDict

# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 900
Fs=100

def parameter(y):
    htbt, rrt=estimate(y)
    
    L=10
    j=1
    T=0.24
    N=len(y)
    for cntr in range(1,N):
        #x[cntr-1]=cntr
        #y[cntr-1]=1*math.sin(314.1416*cntr*T)+3*math.sin(2*314.1416*cntr*T)
        suom=0
        acsum=0

    for cntr in range(1,N):
        suom=suom+y[cntr-1]
        acsum=acsum+y[cntr-1]*y[cntr-1]
    avg=suom/N
    ac=math.sqrt(acsum/N)
    for cntr in range(1,N):
        y[cntr-1]=y[cntr-1]-avg
    d=np.empty(N-L)
    j=1
    for cntr in range(L,N):
        d[j-1]= y[cntr]
        j=j+1
    
    #formation of D
    D=np.empty([N-L, L])
    r=1
    c=1
    for cntr in range(1,N-L+1):
        for cntr1 in range(1,L+1):
            D[r-1,c-1]=y[L-cntr1+cntr-1]
            c=c+1
        c=1
        r=r+1
    inversa=np.linalg.pinv(D)
    a=np.empty(L)
    a=np.dot(inversa,d)
    #obtaining z roots
    z=np.empty(11)
    for cntr in range(0,11):
        if cntr==0:
            z[cntr]=1
        else:
            z[cntr]=-a[cntr-1]
    rots=np.roots(z)

    #formation of y=uc and y=y1
    y1=np.empty(L)
    for cntr in range(0,L):
        y1[cntr]=y[L+cntr-1]


    u=np.empty([L,L],complex)
    c1=np.empty(L)

    r=1
    c=1
    for cntr in range(1,L+1):
        for cntr1 in range(1,L+1):
            u[r-1,c-1]=u[r-1,c-1]+0j
            u[r-1,c-1]= pow(rots[cntr1-1],r-1)
            c=c+1
        c=1
        r=r+1
       
    c1=np.dot(np.linalg.pinv(u),y1)
    #amplitude
        
    val=np.empty(L)
    for cntr in range(0,L-1):
        val[cntr]=abs(c1[cntr]*2)
    #frequency
    f=np.empty(L)
    for cntr in range(0,L-1):
        re=rots[cntr].real
        im=rots[cntr].imag
        kk=2*3.1416*T
        if re!=0:
            f[cntr]=math.atan(im/re)/kk


    phi=np.empty(L)
    for cntr in range(0,L):
        # print c1[cntr].real , c1[cntr].imag
        if c1[cntr].real==0:
            phi[cntr]=cmath.pi/2
        else:
            real=c1[cntr].imag
            imaginary=c1[cntr].real
            if real!=0:
                phi[cntr]=math.atan(imaginary/real)
            else:
                phi[cntr]=0
                val[cntr]=0
                f[cntr]=0
    for cntr in range(0,L):
        if f[cntr]<0.2 or val[cntr]<0.01:
            val[cntr]=0
            phi[cntr]=0
            f[cntr]=0
    val=[x for (y,x) in sorted(zip(f,val))]
    phi=[x for (y,x) in sorted(zip(f,phi))]
    f=sorted(f)
#    for cntr in range(0,9):
#        if f[cntr]!=0:
##            print val[cntr],'   ',f[cntr],'   ',phi[cntr],'   '
#            xx=str(val[cntr])+' '+str(f[cntr])+' '+str(avg) + ' '
##    print '\n'
##    print avg,'   ',avg1,'   ',spO2,'  '
##    print '\n'
    #fk=open("outputparams.txt", 'a', 1)
    #fk.write("\n")
    #fk.write(xx)
    #fk.close()
    ## getting neural trained data
    fileObject = open('trained_net_01_900_0001_5000','r')
    net = pickle.load(fileObject)
##    qw=0
##    qw=qw+0.1
    #printing neural resultsavg1
##    print f[9]
##    clear = "\n" * 100
##    print clear
    
    pout=[f[9],f[8],f[7],f[6],f[5],val[9],val[8],val[7],val[6],val[5],avg]
    print "pout: " + str(pout) 
    bp1 = str(net.activate(pout)).split()
    print bp1
    bp1_dia=str(bp1[1])[:5]
    bp1_sys = str(bp1[2])[:5]
    print "Blood Pressure [normal diastolic < 80, systolic < 140] : " + str(bp1[1]) + " " + str(bp1[2])
    
    return htbt, rrt, bp1_sys,bp1_dia
##................................................................##
    
def estimate(data):
    length=len(data)
    wave=thinkdsp.Wave(ys=data,framerate=Fs)
    spectrum=wave.make_spectrum()
    spectrum_heart=wave.make_spectrum()
    spectrum_resp=wave.make_spectrum()

    fft_mag=list(np.absolute(spectrum.hs))
    fft_length= len(fft_mag)

    spectrum_heart.high_pass(cutoff=0.5,factor=0)
    spectrum_heart.low_pass(cutoff=4,factor=0)
    fft_heart=list(np.absolute(spectrum_heart.hs))

    max_fft_heart=max(fft_heart)
    heart_sample=fft_heart.index(max_fft_heart)
    hr=heart_sample*Fs/length*60

    spectrum_resp.high_pass(cutoff=0.15,factor=0)
    spectrum_resp.low_pass(cutoff=0.5,factor=0)
    fft_resp=list(np.absolute(spectrum_resp.hs))

    max_fft_resp=max(fft_resp)
    resp_sample=fft_resp.index(max_fft_resp)
    rr=resp_sample*Fs/length*60


    if hr<10:
        return hr, 0
    else:
        return hr,rr



def process_data_theirs(data_path):

    current_sample_x = []



    with open(data_path, 'rt') as f:
        reader = csv.reader(f)
        prev_label1 = " "
        prev_label2 = " "
        for row in reader:
            if not row:
                continue
            label1 = int(float(row[0]))
            label2 = int(float(row[1]))
            x = float(row[2])

            # Reset when new label is found
            if (prev_label1 != label1 or prev_label2 != label2):
                prev_label1 = label1
                prev_label2 = label2
                current_sample_x = []

            current_sample_x.append(x)

            # Store when we have the length
            if (len(current_sample_x) >= width):
                
                print "label1: " + str(label1)
                print "label2:" + str(label2)
                #ax = plt.gca()
                #ax.set_ylim([-1, 1])
                #plt.plot(current_sample_x)
		#print current_sample_x
		hr,rr,bp1_sys,bp1_dia = parameter(current_sample_x)
		print "bp1_sys: " + bp1_sys
		print "bp1_dia: " + bp1_dia
                
                #plt.show()
                # sample_list.append(current_sample)
                # label_list.append(label)
                # Shrink current sample based on the overlap ratio
                current_sample_x = current_sample_x[int(len(current_sample_x) * (1.0-percent_overlap)):]


def main():
    #data_path = 'prepareDataTest_all01.csv'
    data_path = 'E:/CARS/PRESSURE/ad/cnn/prepareDataTest_16.csv'

    process_data_theirs(data_path)

if __name__ == '__main__':
    main()