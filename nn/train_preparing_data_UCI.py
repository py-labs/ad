import numpy as np
import os
import sys
import argparse
import glob
import time
import csv
import caffe
import matplotlib.pyplot as plt
import serial
from array import array
from matplotlib import pyplot as plt
import pylab as pl
import math
import cmath
from multiprocessing import Process
import thinkdsp
import adaptfilt as adf
from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import pickle
import scipy as sp
import pandas as pd
import csv as csv
from sklearn.ensemble import RandomForestClassifier
import os
import json
import requests
from collections import OrderedDict
from sklearn.cross_validation import train_test_split


# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 900
Fs=125

def calc_parameter(y):

    htbt, rrt=estimate(y)
    L=10
    j=1
    T=0.24
    N=len(y)
    for cntr in range(1,N):
        #x[cntr-1]=cntr
        #y[cntr-1]=1*math.sin(314.1416*cntr*T)+3*math.sin(2*314.1416*cntr*T)
        suom=0
        acsum=0

    for cntr in range(1,N):
        suom=suom+y[cntr-1]
        acsum=acsum+y[cntr-1]*y[cntr-1]
    avg=suom/N
    ac=math.sqrt(acsum/N)
    for cntr in range(1,N):
        y[cntr-1]=y[cntr-1]-avg
    d=np.empty(N-L)
    j=1
    for cntr in range(L,N):
        d[j-1]= y[cntr]
        j=j+1
    #formation of D
    D=np.empty([N-L, L])
    r=1
    c=1
    for cntr in range(1,N-L+1):
        for cntr1 in range(1,L+1):
            D[r-1,c-1]=y[L-cntr1+cntr-1]
            c=c+1
        c=1
        r=r+1

    inversa=np.linalg.pinv(D)
    a=np.empty(L)
    a=np.dot(inversa,d)
    #obtaining z roots
    z=np.empty(11)
    for cntr in range(0,11):
        if cntr==0:
            z[cntr]=1
        else:
            z[cntr]=-a[cntr-1]
    rots=np.roots(z)

    #formation of y=uc and y=y1
    y1=np.empty(L)
    for cntr in range(0,L):
        y1[cntr]=y[L+cntr-1]


    u=np.empty([L,L],complex)
    c1=np.empty(L)

    r=1
    c=1
    for cntr in range(1,L+1):
        for cntr1 in range(1,L+1):
            u[r-1,c-1]=u[r-1,c-1]+0j
            u[r-1,c-1]= pow(rots[cntr1-1],r-1)
            c=c+1
        c=1
        r=r+1
       
    c1=np.dot(np.linalg.pinv(u),y1)
    #amplitude
        
    val=np.empty(L)
    for cntr in range(0,L-1):
        val[cntr]=abs(c1[cntr]*2)
    #frequency
    f=np.empty(L)
    for cntr in range(0,L-1):
        re=rots[cntr].real
        im=rots[cntr].imag
        kk=2*3.1416*T
        if re!=0:
            f[cntr]=math.atan(im/re)/kk


    phi=np.empty(L)
    for cntr in range(0,L):
        # print c1[cntr].real , c1[cntr].imag
        if c1[cntr].real==0:
            phi[cntr]=cmath.pi/2
        else:
            real=c1[cntr].imag
            imaginary=c1[cntr].real
            if real!=0:
                phi[cntr]=math.atan(imaginary/real)
            else:
                phi[cntr]=0
                val[cntr]=0
                f[cntr]=0
    for cntr in range(0,L):
        if f[cntr]<0.2 or val[cntr]<0.01:
            val[cntr]=0
            phi[cntr]=0
            f[cntr]=0
    val=[x for (y,x) in sorted(zip(f,val))]
    phi=[x for (y,x) in sorted(zip(f,phi))]
    f=sorted(f)
    
    pout=[f[9],f[8],f[7],f[6],f[5],val[9],val[8],val[7],val[6],val[5],avg, htbt, rrt]
    return pout    

def estimate(data):
    length=len(data)
    wave=thinkdsp.Wave(ys=data,framerate=Fs)
    spectrum=wave.make_spectrum()
    spectrum_heart=wave.make_spectrum()
    spectrum_resp=wave.make_spectrum()

    fft_mag=list(np.absolute(spectrum.hs))
    fft_length= len(fft_mag)

    spectrum_heart.high_pass(cutoff=0.5,factor=0)
    spectrum_heart.low_pass(cutoff=4,factor=0)
    fft_heart=list(np.absolute(spectrum_heart.hs))

    max_fft_heart=max(fft_heart)
    heart_sample=fft_heart.index(max_fft_heart)
    hr=heart_sample*Fs*60/length

    spectrum_resp.high_pass(cutoff=0.15,factor=0)
    spectrum_resp.low_pass(cutoff=0.5,factor=0)
    fft_resp=list(np.absolute(spectrum_resp.hs))

    max_fft_resp=max(fft_resp)
    resp_sample=fft_resp.index(max_fft_resp)
    rr=resp_sample*Fs*60/length


    if hr<10:
        return hr, 0
    else:
        return hr,rr


def process_data_theirs(data_path):
    sample_list_x = [] # List of lists

    current_sample_x = []

    label_list1  = []
    label_list2  = []
    dict = {}
    with open(data_path, 'rt') as f:
        reader = csv.reader(f)
        prev_label1 = " "
        prev_label2 = " "
        for row in reader:
            if not row:
                continue
            label1 = int(float(row[0]))
            label2 = int(float(row[1]))
            x = float(row[2])
            if (prev_label1 == " "):
		prev_label1 = label1
                prev_label2 = label2		
            # Reset when new label is found
            if ((prev_label1 != label1 or prev_label2 != label2)): #and prev_label1 != " "):
                prev_label1 = label1
                prev_label2 = label2
                cleanedSignal = medianFilter(current_sample_x)
		sample_list_x.append(cleanedSignal)
       	        label_list1.append(label1)
               	label_list2.append(label2)		
		current_sample_x = []



            current_sample_x.append(x)
            
            # Store when we have the length
            #if (len(current_sample_x) == width):
		#print current_sample_x
		
                #xvu = [label1, label2]
		
		#if str(xvu) in dict:
		#	dict[str(xvu)] += 1
		#else:
		#	dict[str(xvu)] = 0		
		#if (dict[str(xvu)] < 35):
		#	#params = calc_parameter(current_sample_x)		
		#	cleanedSignal = medianFilter(current_sample_x)
		#	sample_list_x.append(cleanedSignal)
        	#       label_list1.append(label1)
                #	label_list2.append(label2)

                # Retain only certain overlap percentages of the sample
                #current_sample_x = current_sample_x[int(len(current_sample_x) * (1.0-percent_overlap)):]
    prev_label1 = label1
    prev_label2 = label2
    #print current_sample_x
    cleanedSignal = medianFilter(current_sample_x)
    #print cleanedSignal
    sample_list_x.append(cleanedSignal)
    #print sample_list_x
    label_list1.append(label1)
    label_list2.append(label2)		
    current_sample_x = []
    return sample_list_x, label_list1, label_list2


def train_data_treirs(sample_list_x, label_list1, label_list2):
	print "start training"
	ds = SupervisedDataSet(13, 2)
	
        for i in range(len(sample_list_x)):
		xlu = sample_list_x[i]
		xvu = [label_list1[i], label_list2[i]]       
        	ds.addSample(xlu, xvu)



	n = buildNetwork(ds.indim,500,ds.outdim,recurrent=True,bias=True)
	t = BackpropTrainer(n,learningrate=0.001,momentum=0.5,verbose=True)
	t.trainUntilConvergence(ds,5000,continueEpochs=5000,validationProportion=0.1)
	t.testOnData(verbose=True)

	fileObject = open('trained_net_001', 'w')
	pickle.dump(n, fileObject)
	fileObject.close()
        #from pybrain.tools.xml.networkwriter import NetworkWriter
	#NetworkWriter.writeToFile(net, sys.argv[1])

	t = BackpropTrainer(n,learningrate=0.0001,momentum=0.5,verbose=True)
	t.trainUntilConvergence(ds,5000,continueEpochs=5000,validationProportion=0.1)
	t.testOnData(verbose=True)

	fileObject = open('trained_net_0001_UCI_htbt_rrt', 'w')
	pickle.dump(n, fileObject)
	fileObject.close()


def train_data_treirs0(sample_list_x, label_list1, label_list2):
	ftrain_input_all = sample_list_x
	ftrain_output_all = []
	print len(ftrain_input_all)
	for i in range(len(sample_list_x)):
		xvu = [label_list1[i], label_list2[i]]       
		ftrain_output_all.append(xvu)
	ftrain_input, ftest_input, ftrain_output, ftest_output = train_test_split(ftrain_input_all, ftrain_output_all, test_size=0.1)        	
	forest=RandomForestClassifier(n_estimators=100)
	forest=forest.fit(ftrain_input, ftrain_output)
	fileObject = open('trained_forest', 'w')
	pickle.dump(forest, fileObject)
	fileObject.close()
	print ftest_input
	for i in range(len(ftest_input)) :
		f_test = np.array(ftest_input[i])
		f_test = f_test.reshape(1, -1)
	        output=forest.predict(f_test)
		print "output: " + str(output) + " real: "  + str(ftest_output[i])

def main():
    data_paths_bp = ['E:/CARS/PRESSURE/data_estimate/txt/bp1_0.txt', 'E:/CARS/PRESSURE/data_estimate/txt/bp2_0.txt','E:/CARS/PRESSURE/data_estimate/txt/bp3_0.txt','E:/CARS/PRESSURE/data_estimate/txt/bp4_0.txt']
    data_paths_ppg = ['E:/CARS/PRESSURE/data_estimate/txt/ppg1_0.txt', 'E:/CARS/PRESSURE/data_estimate/txt/ppg2_0.txt','E:/CARS/PRESSURE/data_estimate/txt/ppg3_0.txt','E:/CARS/PRESSURE/data_estimate/txt/ppg4_0.txt']

    #data_paths_bp = ['E:/CARS/PRESSURE/data_estimate/txt/test2_bp.txt', 'E:/CARS/PRESSURE/data_estimate/txt/test2_bp.txt']
    #data_paths_ppg = ['E:/CARS/PRESSURE/data_estimate/txt/test2_ppg.txt', 'E:/CARS/PRESSURE/data_estimate/txt/test2_ppg.txt']

    sample_list_x, label_list1, label_list2 = process_data_theirs(data_paths_bp, data_paths_ppg)
    #print sample_list_x
    #print label_list1
    train_data_treirs(sample_list_x, label_list1, label_list2)

if __name__ == '__main__':
    main()