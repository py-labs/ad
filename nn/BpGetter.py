#-*- coding: utf-8 -*-
import pickle
import thinkdsp
import numpy as np
from numpy import NaN, Inf, arange, isscalar, asarray, array
from matplotlib.pyplot import plot, scatter, show
import math

class BpGetter:
	def __init__(self, modelFile = 'trained_net_01_900_0001_5000', Fs = 100):
		
		self.modelFile = modelFile
		self.Fs = Fs
		## getting neural trained data
		fileObject = open(self.modelFile,'r')
		#fileObject = open('trained_net_0001','r')
		self.net = pickle.load(fileObject)		


	def getBP(self, ppg, scaleFactor = 1/1000, peakMaximum = 0.1): #1/4500
		y = []
		for p in ppg:
			y.append(p*scaleFactor)
		htbt, rrt = self.estimate(ppg)
		print "htbt: " + str(htbt)
		print "rrt: " + str(rrt)
		htbt, rrt = self.estimate(y)
		print "htbt: " + str(htbt)
		print "rrt: " + str(rrt)
		
		maxtab, mintab = self.peakdet(y, peakMaximum)
		plot(y)
	    	scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
	    	scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
    		show()
    		difference = self.getDiffenence(mintab, maxtab)
		print "difference: " + str(difference)
		L = 10
		j = 1
		T = 0.24
		N = len(y)
		
		for cntr in range(1,N):
			suom=0
			acsum=0

		for cntr in range(1,N):
			suom = suom + y[cntr-1]
			acsum = acsum + y[cntr-1]*y[cntr-1]
		avg = suom/N
		ac = math.sqrt(acsum/N)
		for cntr in range(1,N):
			y[cntr - 1] = y[cntr - 1] - avg
		d = np.empty(N-L)
		j=1
		for cntr in range(L,N):
			d[j - 1] = y[cntr]
			j = j + 1
	
		#formation of D
		D = np.empty([N - L, L])
		r = 1
		c = 1
		for cntr in range(1, N - L + 1):
			for cntr1 in range(1, L + 1):
				D[r - 1, c - 1] = y[L - cntr1 + cntr - 1]
				c = c + 1
			c = 1
			r = r + 1
		inversa = np.linalg.pinv(D)
		a = np.empty(L)
		a = np.dot(inversa, d)
		#obtaining z roots
		z = np.empty(11)
		for cntr in range(0, 11):
			if cntr == 0:
				z[cntr] = 1
			else:
				z[cntr] = -a[cntr - 1]
		rots = np.roots(z)

		#formation of y=uc and y=y1
		y1 = np.empty(L)
		for cntr in range(0,L):
			y1[cntr] = y[L + cntr - 1]


		u = np.empty([L, L], complex)
		c1 = np.empty(L)
	
		r = 1
		c = 1
		for cntr in range(1, L + 1):
			for cntr1 in range(1, L + 1):
				u[r - 1, c - 1] = u[r - 1, c - 1] + 0j
				u[r - 1, c - 1] = pow(rots[cntr1 - 1], r - 1)
				c = c+1
			c = 1
			r = r+1
		
		c1 = np.dot(np.linalg.pinv(u), y1)
		#amplitude
		val = np.empty(L)
		for cntr in range(0, L-1):
			val[cntr] = abs(c1[cntr]*2)
		#frequency
		f = np.empty(L)
		for cntr in range(0, L-1):
			re = rots[cntr].real
			im = rots[cntr].imag
			kk = 2*3.1416*T
			if re != 0:
				f[cntr] = math.atan(im/re)/kk
	
	
		phi = np.empty(L)
		for cntr in range(0,L):
			# print c1[cntr].real , c1[cntr].imag
			if c1[cntr].real == 0:
				phi[cntr] = cmath.pi/2
			else:
				real = c1[cntr].imag
				imaginary = c1[cntr].real
				if real != 0:
					phi[cntr] = math.atan(imaginary/real)
				else:
					phi[cntr] = 0
					val[cntr] = 0
					f[cntr] = 0
		for cntr in range(0,L):
			if f[cntr] < 0.2 or val[cntr] < 0.01:
				val[cntr] = 0
				phi[cntr] = 0
				f[cntr] = 0
		val = [x for (y,x) in sorted(zip(f, val))]
		phi = [x for (y,x) in sorted(zip(f, phi))]
		f = sorted(f)
		
	
		pout=[f[9],f[8],f[7],f[6],f[5],val[9],val[8],val[7],val[6],val[5],avg]
		print "pout: " + str(pout) 
		#if (pout[0] >= 0.01 and pout[0] <=1 and difference >= 0.1 and difference <= 0.6):
		if (difference >= 0.1 and difference <= 0.6):
			bp1 = str(self.net.activate(pout)).split()
			#print bp1
			bp1_dia = str(bp1[1])[:5]
			bp1_sys = str(bp1[2])[:5]
			#print "Blood Pressure [normal diastolic < 80, systolic < 140] : " + str(bp1[1]) + " " + str(bp1[2])
		else:
			bp1_dia = 0
			bp1_sys = 0
		
		return bp1_sys, bp1_dia

		
	def estimate(self, data):
		length = len(data)
		wave = thinkdsp.Wave(ys = data,framerate = self.Fs)
		spectrum = wave.make_spectrum()
		spectrum_heart = wave.make_spectrum()
		spectrum_resp = wave.make_spectrum()
	        
	        #print wave.duration
		fft_mag = list(np.absolute(spectrum.hs))
		fft_length = len(fft_mag)
	
		spectrum_heart.high_pass(cutoff = 0.8, factor = 0.001)
		spectrum_heart.low_pass(cutoff = 2, factor = 0.001)
		fft_heart = list(np.absolute(spectrum_heart.hs))
	
		max_fft_heart = max(fft_heart)
		heart_sample = fft_heart.index(max_fft_heart)
		hr = heart_sample*self.Fs*60/length
	
		spectrum_resp.high_pass(cutoff = 0.15, factor = 0)
		spectrum_resp.low_pass(cutoff = 0.5, factor = 0)
		fft_resp = list(np.absolute(spectrum_resp.hs))
	
		max_fft_resp = max(fft_resp)
		resp_sample = fft_resp.index(max_fft_resp)
		rr = resp_sample*self.Fs*60/length
	
	        #print hr
		#print rr
		#print resp_sample
		#print self.Fs
		#print length
		#print '60'
				
		if hr < 10:
			return hr, 0
		else:
			return hr, rr
	
	def peakdet(self, v, delta, x = None):
		maxtab = []
		mintab = []
		   
		if x is None:
			x = arange(len(v))
		
		v = asarray(v)
		
		if len(v) != len(x):
			sys.exit('Input vectors v and x must have same length')
		
		if not isscalar(delta):
			sys.exit('Input argument delta must be a scalar')
		
		if delta <= 0:
			sys.exit('Input argument delta must be positive')
		
		mn, mx = Inf, -Inf
		mnpos, mxpos = NaN, NaN
		
		lookformax = True
		
		for i in arange(len(v)):
			this = v[i]
			if this > mx:
				mx = this
				mxpos = x[i]
			if this < mn:
				mn = this
				mnpos = x[i]
		    	
			if lookformax:
				if this < mx-delta:
					maxtab.append((mxpos, mx))
					mn = this
					mnpos = x[i]
					lookformax = False
			else:
				if this > mn+delta:
					mintab.append((mnpos, mn))
					mx = this
					mxpos = x[i]
					lookformax = True

		return array(maxtab), array(mintab)

	
	
    	def getDiffenence(self,mintab, maxtab):
		countMax = len(maxtab)
		countMin = len(mintab)
		
		
		countDiff = 0
		sumDiff = 0
		for i in range(min(countMax, countMin)):
			countDiff += 1
			sumDiff	+= abs(maxtab[i][1] - mintab[i][1])
		
		if (countDiff != 0 and min(countMax, countMin) > 5):
			
			return (float)(sumDiff)/countDiff
		#print countMax
		#print countMin
		#arr = []
		#for i in range(min(countMax, countMin)):
		#	arr.append(abs(maxtab[i][1] - mintab[i][1]))
		#print arr			

		return 0
