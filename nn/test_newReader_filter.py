import TextBaseReader
import BpGetter
import numpy as np
import thinkdsp
import math
import cmath
import pickle
#from HRClassifier import HRClassifier
from scipy import signal
import matplotlib.pyplot as plt

# Percent overlap between subsequent samples
percent_overlap = 0.50

# Filter param
height = 1
width = 1500

#c = HRClassifier()

#c.train(False)


path = "E:/CARS/PRESSURE/test_data/2017-04-07-14-51-20.csv"
#path = "E:/CARS/PRESSURE/Pulse-out/2017-03-17-11-04-05.csv"
#path = "E:/CARS/PRESSURE/uqvitalsignsdata_case01to32/uqvitalsignsdata/test15/uq_vsd_case31_fulldata_11.csv"
textBaseReader = TextBaseReader.TextBaseReader(path)
PPG = textBaseReader.getPPG()



#bpGetter = BpGetter.BpGetter('trained_net_01_900_0001_5000', 100)
#bpGetter = BpGetter.BpGetter('trained_net_01_900_0001_5000_UCI', 100)

#process_data_theirs(PPG, width, bpGetter)

#results = np.array(c.getValidHRranges(PPG))

#for i in results:
#	print(i) # print heart rate value



# Filter outliers using a moving median filter
dataWindow = signal.medfilt(PPG, 3)	

plt.plot(dataWindow, color='blue')
plt.plot(PPG, color='red')
plt.ylabel('HRV')
plt.show()