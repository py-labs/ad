
deviation_sys = [0.0]*100
deviation_dias = [0.0]*100
deviation_all = [0.0]*100

f = open('err.txt', 'r')

count = 0
for line in f:
	strs = line.split(' ')
	dpe = float(strs[0])
	spe = float(strs[1])
	#print spe
	count += 1
	for i in range(100):
		if (dpe < i):
			deviation_dias[i] += 1
		if (spe < i):
			deviation_sys[i] += 1
		if (spe < i and dpe < i):
			deviation_all[i] += 1

for i in range(100):
	deviation_dias[i] = deviation_dias[i]/count				
	deviation_sys[i] = deviation_sys[i]/count
	deviation_all[i] = deviation_all[i]/count

print "Diastole"
for i in range(100):
	print deviation_dias[i]

print "Systole"
for i in range(100):
	print deviation_sys[i]

print "All"
for i in range(100):
	print deviation_all[i]